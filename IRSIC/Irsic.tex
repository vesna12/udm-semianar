% **************************************
%       BESEDILO SEMINARSKE NALOGE
% **************************************

Pojem homomorfizma srečamo v algebri kot preslikavo, ki ohranja operacijo. Podobno je tudi pri grafih -- homomorfizem je preslikava med grafoma, ki ohranja relacijo povezanosti vozlišč. S pomočjo homomorfizmov lahko preučujemo različne lastnosti grafov, na primer barvanja, ožine in lastnosti produktov grafov. 

V nadaljevanju najprej predstavimo osnovne definicije in nekaj ilustrativnih zgledov. V razdelku \ref{razd2} spoznamo relacijo, ki jo podajajo homomorfizmi. Nato si ogledamo lastnosti retrakta, ki je le posebna vrsta homomorfizma. V razdelku \ref{razd4} pridobljeno znanje povežemo z barvanji grafov, nato pa v zadnjem razdelku spoznamo še pojem jedra. 

\section{Osnovni pojmi}
\label{razd1}

Oglejmo si formalno definicijo homomorfizma.

\begin{definition}
Naj bosta $G$ in $H$ grafa. Preslikava $f \colon V(G) \to V(H)$ je \emph{homomorfizem}, če velja $uv \in E(G) \implies f(u)f(v) \in E(H)$.
\end{definition}

V nadaljevanju pogosto zlorabimo notacijo in pišemo homomorfizem kar kot $f \colon G \to H$.

Spomnimo se še nekaj drugih definicij.

\begin{definition}
Naj bosta $G$ in $H$ grafa. Preslikava $f \colon V(G) \to V(H)$ je \emph{izomorfizem}, če velja $uv \in E(G) \iff f(u)f(v) \in E(H)$. Če je še dodatno $G = H$, preslikavi pravimo \emph{avtomorfizem}.

\emph{Pravi podgraf} grafa $G$ je podgraf, ki ni prazen in hkrati ni enak grafu $G$.

Relacijo povezanosti v grafu $G$ označimo kot $u \sim_G v$ in pomeni, da je $uv \in E(G)$.
\end{definition}

\begin{remark} Direktno iz definicij sledi naslednje:
\begin{itemize}
\item Vsak izomorfizem ali avtomorfizem grafov je tudi homomorfizem.
\item Če grafa $G$ in $H$ nimata zank, potem za vsak homomorfizem $f \colon G \to H$ sledi: $uv \in E(G) \implies f(u) \neq f(v)$.
\end{itemize}
\end{remark}

Ves čas obravnavamo enostavne (in končne) grafe. 

\begin{example}
Da si homomorfizme lažje predstavljamo, si oglejmo nekaj zgledov.
\begin{itemize}
\item Vložitev podgrafa v graf je vedno homomorfizem.
\item Naj bo $G$ dvodelni graf in $V(G) = A \cup B$ ustrezna particija vozlišč ($A, B$ disjunktni in neprazni). Tedaj je preslikava $f \colon G \to K_2$, definirana kot
$$f(u) = \begin{cases}
1 \; \text{, če }u \in A\\
2 \; \text{, če }u \in B,
\end{cases}$$
kjer $K_2$ enačimo s povezavo z vozliščema $1$ in $2$. Preverimo, da je to res homomorfizem. Če je $uv \in E(G)$, potem je (brez škode za splošnost) $u \in A, v\in B$. Sledi, da je $f(u) = 1, f(v) = 2$ in torej $f(u)f(v) \in E(K_2)$.
% V nadaljevanju bomo videli, da je graf dvodelen natanko tedaj, ko obstaja homomorfizem iz njega v povezavo $K_2$. To sledi zato, ker so dvodelni grafi natanko grafi, ki jih lahko pobarvamo z dvema barvama, in iz 

Kadar graf $G$ vsebuje vsaj eno povezavo, lahko definiramo tudi homomorfizem v obratni smeri, to je $g \colon K_2 \to G$, kot $g(1) = a, g(2) = b$, kjer sta $a \in A, b \in B$ ter $ab \in E(G)$. 

Med grafoma $G$ in $K_2$ torej obstajata homomorfizma v obe smeri, vendar si grafa nista izomorfna (čim ima $G$ vsaj 3 vozlišča).
\item Označimo vozlišča cikla $C_7$ zaporedoma z $A, B, C, D, E, F, G$ in vozlišča cikla $C_5$ zaporedoma z $A, B, C, D, E$. Oglejmo si preslikavo $h \colon C_7 \to C_5$, definirano kot 
\begin{align*}
h|_{\{A, B, C\}} & = \id\\
h(D) = h(F) & = D\\
h(E) = h(G) & = E.
\end{align*}
Po definiciji hitro vidimo, da je $h$ homomorfizem. Analogno bi lahko definirali homomorfizem $C_{n+2} \to C_n$ za splošen $n \geq 3$.
\begin{figure}
    \begin{centering}
    \includegraphics[width=6cm]{IRSIC/skicaUDM.png}
    \caption{Homomorfizem med različno velikima cikloma.}
    \label{slika:cikli}
    \end{centering}
\end{figure}
\end{itemize}
\end{example}

Vprašamo se lahko, ali v tem zadnjem primeru obstaja homomorfizem tudi v obratni smeri, tj.\ $C_n \to C_{n+2}$. Občutek nam pravi, da to ne bo mogoče, ker bi morali manjši cikel nekako ``pretrgati''. Oglejmo si problem kar v splošnem. Najprej potrebujemo naslednjo definicijo.

\begin{definition}
\emph{Ožina} grafa je dolžina najkrajšega cikla vsebovanega v njem. Če graf ne vsebuje nobenega cikla, je njegova ožina neskončno. Podobno definiramo tudi \emph{sodo} in \emph{liho ožino} kot dolžino najkrajšega sodega oz.\ lihega cikla v grafu.
\end{definition}

V primeru cikla je ožina seveda enaka njegovi dolžini. Ožina poljubnega polnega grafa na $n > 2$ točkah je tri, ker vedno vsebuje trikotnik. V drevesih ni nobenega cikla, zato je njihova ožina enaka neskončno. Kot vemo, je ožina invarianta grafa. Sedaj pa posplošimo ugotovitev iz zgornjega zgleda.

\begin{proposition}
Naj bosta $G$ in $H$ ($G$ ni dvodelen) ter $f \colon G \to H$ homomorfizem med njima. Tedaj je liha ožina grafa $G$ večja ali enaka lihi ožini grafa $H$. 
\end{proposition}

\begin{proof}
Naj bo $C = c_1 c_2 \ldots c_k$ najkrajši lihi cikel v $G$. Dokazati moramo, da v grafu $H$ obstaja lih cikel dolžine $\leq k$. Ker graf $G$ ni dvodelen, ga ne moremo slikati v povezavo izomorfno $K_2$ (to sledi iz posledice \ref{posl:chi}, ki jo bomo spoznali v nadaljevanju). Ker je $C$ cikel, je tudi $f(C)$ cikel, čeprav je morda izrojen (lahko so kakšne povezave in vozlišča združena). Torej $f(C)$ v grafu $H$ podaja vsaj kakšen cikel, in sicer morajo biti vsi ti cikli očitno dolžine $\leq k$. Razmisliti moramo le, da se ne more zgoditi, da bi pri tem dobili same cikle sode dolžine. Gotovo dobimo vsaj kak cikel, ker je $C$ lihe dolžine in ga ne moremo slikati v nek acikličen graf (spet po posledici \ref{posl:chi}). Štejmo dolžino cikla tako, da upoštevamo tudi izrojene povezave, ki se vse pojavijo sodokrat. Če bi bil $f(C)$ sestavljen iz samih sodih ciklov (nekako združenih med sabo), bi sledilo, da je $f(c_1) f(c_2) \ldots f(c_k)$ izrojeni cikel sode dolžine, kar pa ni možno, ker je $k$ lih. 
\end{proof}

\begin{remark} Zanimivo je opaziti: 
\begin{itemize}
\item Zahtevati moramo, da graf $G$ ni dvodelen, sicer ga lahko homomorfno slikamo v povezavo znotraj grafa $H$. 
\item Podobna trditev ne velja za sodo ožino, ker lahko najkrajši sodi cikel v celoti preslikamo v povezavo znotraj grafa $H$, ne glede na to, kakšna je ožina tega grafa.
\end{itemize}
\end{remark}

Iz trditve direktno sledi:

\begin{corollary}
Naj bosta $G$ in $H$ ($G$ ni dvodelen). Če je liha ožina grafa $H$ strogo večja od lihe ožine grafa $G$, potem ne obstaja homomorfizem $G \to H$.
\end{corollary}

Posledica pove, da res ne obstaja homomorfizem iz $C_n$ v $C_{n+2}$, kadar je $n$ liho število. 

V nadaljevanju potrebujemo nekaj pojmov.

Če je $f \colon G \to H$ homomorfizen grafov, za $v \in V(H)$ definiramo $f^{-1}(v) = \{u \in V(G)\; ; \; f(u) = v\}$ in ga imenujemo \emph{vlakno} homomorfizma $f$. Če sta grafa enostavna, tvori $f^{-1}(v)$ neodvisno množico v grafu $G$, tj.\ množico vozlišč, ki so paroma nepovezana. Vlakna homomorfizma $f$ določajo particijo oz.\ razčlenitev $\pi$ množice $V(G)$, ki jo imenujemo \emph{jedro} homomorfizma $f$. Particijo sestavljajo neodvisne množice. 

\begin{example}
Pomen pojmov si oglejmo na zgledih dvodelnega grafa in cikla.
\begin{itemize}
\item Vlakni homomorfizma $f$ sta $f^{-1}(1) = A$ in $f^{-1}(2) = B$, ki sta res neodvisni množici. Jedro homomorfizma $f$ je torej $\pi = \{A, B\}$, ki je res particija množice $V(G)$. 
 
 Vlakna homomorfizma $g$ so oblike $$g^{-1} (u) = \begin{cases}
\{1\} \; \text{, če }u \in A\\
\{2\} \; \text{, če }u \in B,
\end{cases}$$ kar sta tudi neodvisni množici. Jedro je particija $V(K_2) = \{1\} \cup \{2\}$. 
\item Vlakna homomorfizma $h$ so množice z enim ali dvema elementoma in podajajo particijo $V(C_7) = \{A\} \cup \{B\} \cup \{C\} \cup \{D,F\} \cup \{E,G\}$, kot je z barvami nakazano na sliki \ref{slika:cikli}. 
\end{itemize}
\end{example}

\section{Relacija $\to$}
\label{razd2}

Definiramo relacijo $\to$ na sledeč način: $G \to H$ (beri: ``ima homomorfizem v'') natanko tedaj, ko obstaja homomorfizem iz $G$ v $H$. 

\begin{proposition}
\label{trd:tranzitivnost}
Relacija $\to$ je refleksivna in tranzitivna.
\end{proposition}

\begin{proof}
Identična preslikava $G \to G$ je očitno homomorfizem, zato je relacija refleksivna.

Naj bosta $f \colon G \to H$ in $g \colon H \to Z$ homomorfizma med grafi. Preverimo, da je $g \circ f \colon G \to Z$ tudi homomorfizem. Naj bosta $u, v \in V(G), u \sim_G v$. Ker je $f$ homomorfizem, je $f(u) \sim_H f(v)$. Ker je $g$ homomorfizem je $g(f(u)) \sim_Z g(f(v))$, kar smo želeli dokazati.
\end{proof}

Relacija $\to$ ni nujno antisimetrična (glej primer zgoraj z dvodelnim grafom), torej tudi ni delna urejenost.

\begin{definition}
Grafa $G$ in $H$ sta \emph{homomorfno ekvivalentna}, če obstajata homomorfizem $G \to H$ in homomorfizem $H \to G$.
\end{definition}

Očitno je homomorfna ekvivalenca ekvivalenčna relacija, zato podaja ekvivalenčne razrede grafov. Nanje lahko naravno razširimo relacijo $\to$. Opazimo, da je relacija $\to$ delna urejenost na ekvivalenčnih razredih homomorfne ekvivalentnosti grafov.

Če sta grafa homomorfno ekvivalentna, še nista nujno izomorfna. Primer: dvodelni graf in $K_2$ (glej zgled zgoraj).

\section{Retrakti}
\label{razd3}

V nadaljevanju potrebujemo tudi homomorfizme, ki na nekem podgrafu mirujejo. Oglejmo si njihovo formalno definicijo.

\begin{definition}
\emph{(Prava) retrakcija} je homomorfizem $f \colon G \to H$, kjer je $H$ (pravi) podgraf $G$ in je $f|_H$ identična preslikava. Tedaj $H$ imenujemo \emph{(pravi) retrakt} grafa $G$.
\end{definition}

Oglejmo si preprost zgled retrakta.

\begin{example}
Naj bo $X$ graf kot na sliki \ref{slika:petkotnik}. Definirajmo preslikavo $f \colon X \to C$, kjer je $C$ zunanji 5-cikel grafa $X$, kot 
\begin{align*}
f|_C & = \id_C\\
f(F) & = B\\
f(G) & = C\\
f(H) & = D\\
f(I) & = E\\
f(J) & = A.
\end{align*}
Tako definirana preslikava je očitno homomorfizem grafov. Ker pa je $f$ na $C$ identiteta, je celo retrakcija. Torej je $C$ retrakt grafa $X$. Podobno bi lahko graf $X$ retrahirali tudi na notranji 5-cikel.
\begin{figure}
    \begin{centering}
    \includegraphics[width=3.5cm]{IRSIC/petkotnik.png}
    \caption{Graf, ki ga lahko retrahiramo na cikel $C_5$.}
    \label{slika:petkotnik}
    \end{centering}
\end{figure}
Geometrijsko si lahko predstavljamo potek retrakcije tako, da notranji 5-cikel zavrtimo v pozitivni smeri, ga povečujemo in prečne povezave med obema cikloma prilepimo na zunanji cikel. Opozoriti velja, da morda bolj naravna predstava, ko bi notranji cikel le napihnili in torej slikali $F \mapsto A, G \mapsto B$ itd.\ ni retrakcija (ker niti ni homomorfizem, saj sta na primer vozlišči $A$ in $F$ v začetnem grafu povezani, v končnem pa ne).
\end{example}


\section{Povezava z barvanji grafov}
\label{razd4}
Spomnimo se, da je barvanje vozlišč grafa $G$ preslikava $b$ iz $V(G)$ v množico barv, pri čemer sosednji vozlišči ne smeta biti pobarvani z enako barvo. Če lahko $G$ pobarvamo s $k$ različnimi barvami, rečemo, da je $G$ $k$-obarvljiv in $b$ $k$-barvanje. Najmanjše število $k$, da lahko $G$ pobarvamo s $k$ različnimi barvami imenujemo kromatično število grafa $G$ in označimo s  $\chi(G)$. Barvanje grafa določa particijo vozlišč na neodvisne množice (v neki množici so vozlišča iste barve). Smiselno se zdi, da lahko to particijo povežemo z jedrom nekega homomorfizma. S tem v mislih si oglejmo še en zgled retrakcije.

\begin{example}
Naj bo $H$ graf na sliki \ref{slika:pobarvan}. Če vozlišča enake barve slikamo v enako pobarvana vozlišča v notranji 5-kliki, očitno dobimo homomorfizem, ki je poleg tega še retrakcija na kliko.
\begin{figure}
    \begin{centering}
    \includegraphics[width=3.5cm]{IRSIC/pobarvan.png}
    \caption{Barvanje danega grafa podaja rekrakcijo.}
    \label{slika:pobarvan}
    \end{centering}
\end{figure}
\end{example}

Zgled lahko posplošimo:

\begin{proposition}
\label{trd:klika}
Če ima graf kliko velikosti $n$, potem vsako $n$-barvanje grafa podaja retrakcijo na $n$-kliko. 
\end{proposition}

\begin{proof}
Naj bo $G$ graf s $n$-kliko $K_n \subseteq G$ in $b$ $n$-barvanje grafa (barve označimo z $\{1, 2, \ldots, n\}$). Ker je $K_n$ klika, morajo biti vsa njena vozlišča pobarvana z različnimi barvami, torej lahko njena vozlišča brez škode označimo z $\{1, 2, \ldots, n\}$ (ustrezno glede na barvo vozlišč). Definirajmo preslikavo $f \colon G \to K_n$ kot $f(u) = b(u)$. Najprej preverimo, da je preslikava $f$ homomorfizem. Če je $uv \in E(G)$, sta vozlišči govoto pobarvani z različnimi barvami. Njuni sliki $f(u), f(v)$ ležita v $K_n$ kliki, torej sta gotovo povezani. Preslikava $f$ je po definiciji identiteta na kliki $K_n$, zato je retrakcija.
\end{proof}

Če graf ne vsebuje klike, potem homomorfizem, ki ga podaja barvanje, seveda ne bo retrakcija na poln graf. Razmislimo pa lahko, da vseeno dobimo homomorfizem, in poskušamo opaziti povezavo s kromatičnim številom grafa.

\begin{example}
Vemo, da lahko Petersenov graf pobarvamo s tremi barvami (kot na primer na sliki \ref{slika:barvanje}). Barvanje določa particijo vozlišč na neodvisne množice. Če definiramo homomorfizem, ki ima dano particijo za jedro, dobimo homomorfizem iz Petersenovega grafa v poln graf na 3 vozliščih, to je v $K_{\text{število barv}}$.
\begin{figure}
    \begin{centering}
    \includegraphics[width=7cm]{IRSIC/barvanje2.png}
    \caption{Barvanje Petersenovega grafa homomorfizem.}
    \label{slika:barvanje}
    \end{centering}
\end{figure}
\end{example}

Ugotovitve iz zgleda posplošimo:

\begin{proposition}
Kromatično število grafa $G$ je enako najmanjšemu naravnemu številu $r$, da obstaja homomorfizem iz $G$ v $K_r$.
\end{proposition}
\begin{proof}
Naj bo $\chi(G) = r$. Dokažimo, da obstaja homomorfizem $G \to K_r$ in ne obstaja homomorfizem $G \to K_k$ za $k < r$.

Denimo, da obstaja homomorfizem $f \colon G \to K_k$. Preslikava ima $k$ vlaken, ki tvorijo particijo $V(G)$ na neodvisne množice, torej določajo $k$-barvanje grafa $G$. Torej v tem primeru sledi $r = \chi(G) \leq k$, kar je protislovje.

Ker $G$ lahko pobarvamo z $r$ barvami, je preslikava $G \to K_r$, ki v isto vozlišče slika natanko vozlišča iste barve, homomorfizem.
\end{proof}

\begin{corollary}
Naj bosta $G$ in $H$ grafa in $f \colon G \to H$ homomorfizem med njima. Tedaj je $\chi(G) \leq \chi(H)$.
\end{corollary}

\begin{proof}
Po prejšnji trditvi obstaja homomorfizem $g \colon H \to K_{\chi(H)}$. Tedaj je $g \circ f \colon G \to K_{\chi(H)}$ tudi homomorfizem in po prejšnji trditvi sledi, da je $\chi(G) \leq \chi(H)$.
\end{proof}

Če zadnjo posledico negiramo, sledi:

\begin{corollary}
\label{posl:chi}
Če za dana grafa $G$ in $H$ velja $\chi(G) > \chi(H)$, potem ne obstaja noben homomorfizem iz $G$ v $H$.
\end{corollary}

\section{Jedra}
\label{razd5}

Nekateri grafi (ali njihovi podgrafi) se pri slikanju s homomorfizmom obnašajo na poseben način -- se na nek način ohranjajo. Pravimo jim jedra in jih formalno definiramo na sledeč način.

\begin{definition}
Graf $G$ je \emph{jedro} (anlg.\ core), če je vsak homomorfizem $G \to G$ bijekcija.

Podgraf $H \subseteq G$ je \emph{jedro grafa} G, če je $H$ jedro in obstaja homomorfizem $G \to H$. Tedaj označimo $H = G^\bullet$.
\end{definition}

% \begin{remark}
% Ekvivalentno lahko definiramo, da je $G$ jedro, če ima najmanjše število vozlišč med vsemi grafi v svojem ekvivalenčnem razredu glede na homomorfno ekvivalenco.
% \end{remark}
% Dokaz??

Zgoraj definiran pojem jedra se razlikuje od definicije v uvodu. Vendar je iz konteksta vedno razvidno, ali govorimo o jedru -- particiji ali o jedru -- (pod)grafu. 

\begin{example}
Oglejmo si nekaj zgledov.
\label{zgled}
\begin{itemize}
\item Poln graf $K_n$ je jedro. Poljuben homomorfizem samega vase je namreč le permutacija vozlišč, zato je bijekcija.
\item Kolesa $W_{2n+1}$ so jedra. Naj bo $f \colon W_{2n+1} \to W_{2n+1}$ poljuben homomorfizem. Nobeno od vozlišč s cikla se ne more slikati v središče in ker je središče povezano z vsemi vozlišči na ciklu, je edina možnost, da se središče slika samo vase. Preostali del preslikave $f$ pa je potem le homomorfizem cikla $C_{2n+1}$. V nadaljevanju bomo videli, da so lihi cikli jedra, iz česar sledi, da je $f$ bijekcija.
\item Petersenov graf je jedro (dokaz preko lastnosti Kneserjevih grafov).
\item Sodi cikli $C_{2n} (n \geq 2)$ niso jedra. Ker so sodi cikli dvodelni grafi, obstaja homomorfizem iz cikla v povezavo $K_2 \subset C_{2n}$, ki očitno ni injektiven, torej tudi ni bijektiven (glej sliko \ref{slika:sodCikel}).
\begin{figure}[h!]
    \begin{centering}
    \includegraphics[width=7cm]{IRSIC/sodCikel.png}
    \caption{Če slikamo tako, kot nakazujejo barve vozlišč na sliki, dobimo homomorfizem $C_{2n} \to C_{2n}$, ki ni bijektiven.}
    \label{slika:sodCikel}
    \end{centering}
\end{figure}
\item Poljuben dvodelen graf z vsaj tremi vozlišči in vsaj eno povezavo ni jedro.
\item Polni graf brez ene povezave ni jedro. Vozlišči, ki nista povezani, lahko pobarvamo z isto barvo, nato pa homomorfizem definiramo tako, da je na ostalih vozliščih identiteta, na nepovezanih vozliščih pa slikamo v izbranega predstavnika barvnega razreda. Preslikava je homomorfizem, ki ni injektiven (glej sliko \ref{slika:polnBrez}).
\begin{figure}[h!]
    \begin{centering}
    \includegraphics[width=7cm]{IRSIC/polnBrez.png}
    \caption{Če slikamo tako, kot nakazujejo barve vozlišč na sliki, dobimo homomorfizem, ki ni bijektiven.}
    \label{slika:polnBrez}
    \end{centering}
\end{figure}
\end{itemize}
\end{example}

Opazimo, da direktno iz definicije sledi, da če je $G^\bullet$ jedro grafa $G$, obstajata homomorfizem $G^\bullet \to G$ (vložitev) in homomorfizem $G \to G^\bullet$ (homomorfizem iz definicije). To lastnost bomo potrebovali v nadaljevanju.

Navedimo ekvivalentni definiciji jedra, ki nam bosta pomagali bolje razumeti jedra.

\begin{proposition}
\label{trd:def}
Naslednje trditve so ekvivalentne:
\begin{enumerate}
\item Graf $G$ je jedro.
\item Ne obstaja homomorfizem iz $G$ v njegov pravi podgraf.
\item Graf $G$ nima nobenega pravega retrakta.
\end{enumerate}
%Graf $G$ je jedro natanko tedaj, ko $G$ nima nobenega pravega retrakta.
\end{proposition}

\begin{proof}
Implikaciji $(1) \implies (3)$ in $(3) \implies (2)$ sta očitni. Dokazati moramo le še $(2) \implies (1)$.  Naj bo $f \colon G \to G$ poljuben homomorfizem. Po predpostavki $(2)$ mora biti surjektiven. Ker je graf $G$ končen, sledi tudi bijektivnost.
% $(\Rightarrow)$ Velja po definiciji jedra.
% 
% $(\Leftarrow)$ Naj bo $f \colon G \to G$ poljuben homomorfizem. Dokazati moramo, da je bijekcija. Zožitev preslikave $f \colon G \to f(G) \subseteq G$ je gotovo surjektivna. Ker je graf končen, od tod sledi, da je tudi injektivna, zato mora veljati $|G| \leq |f(G)|$, kar pa je možno je v primeru, ko je $f(G) = G$, torej je celotni homomorfizem $f$ surjektiven in zato tudi bijektiven.
\end{proof}

Oglejmo si še dodatno lastnost jedra danega grafa. 

\begin{proposition}
Naj bo $H$ jedro grafa $G$. Tedaj je $H$ retrakt $G$.
\end{proposition}

Ta lastnost jedra seveda ne karakterizira, ker ni nobenega razloga, da bi bil poljuben retrakt tudi jedro grafa. Oglejmo si sedaj dokaz trditve.

\begin{proof}
Ker je $H$ jedro $G$, obstaja homomorfizem $f \colon G \to H$, da je $f|_H$ bijekcija. Torej je $f|_H \colon H \to H$ avtomorfizem. Zato obstaja njegov inverz $g$, ki je tudi avtomorfizem. Tedaj je po trditvi \ref{trd:tranzitivnost} preslikava $f \circ g \colon G \to H$ homomorfizem grafov. Poleg tega je $(f \circ g)|_H (v) = f(g(v)) = v$, torej je $(f \circ g)|_H = \id_H$, iz česar sledi, da je $H$ retrakt od $G$.
\end{proof}

V nadaljevanju bi radi videli, da vsak graf ima jedro, ki je v nekem smislu enolično. Potrebujemo naslednjo lemo.

\begin{lemma}
\label{trd:izo}
Naj bosta $G$ in $H$ jedri. Grafa $G$ in $H$ sta homomorfno ekvivalentna natanko tedaj, ko sta izomorfna.
\end{lemma}

\begin{proof}
$(\Rightarrow)$ Naj bosta $f \colon G \to H$ in $g \colon H \to G$ homomorfizma med grafoma. Potem je $f \circ g \colon H \to H$ tudi homomorfizem, ki je bijektiven, saj je $H$ jedro. Podobno sklepamo, da je $g \circ f$ bijekcija. Sledi, da sta si preslikavi $f$ in $g$ inverzni in obe bijektivni, torej sta grafa $G$ in $H$ izomorfna. 

$(\Leftarrow)$ Očitno, saj je izomorfizem hkrati tudi homomorfizem in ima inverz, ki je tudi homomorfizem.
\end{proof}

Iz leme sledi, da je relacija $\to$ delna urejenost na izomorfnih razredih jeder. Pred dokazom izreka o obstoju in enoličnosti jedra, si oglejmo še povezavo med jedri grafov in homomorfno ekvivalenco.

\begin{proposition}
Grafa $G$ in $H$ sta homomorfno ekvivalentna natanko tedaj, ko sta njuni jedri izomorfni.
\end{proposition}

\begin{proof}
Dokazati moramo: grafa $G$ in $H$ sta homomorfno ekvivalentna natanko tedaj, ko sta njuni jedri homomorfno ekvivalentni. Potem trditev velja po lastnosti \ref{trd:izo}.

$(\Rightarrow)$ Če obstaja homomorfizem $G \to H$, potem imamo sledeče zaporedje homomorfizmov:
$$G^\bullet \to G \to H \to H^\bullet,$$
ki tvori homomorfizem iz $G^\bullet$ v $H^\bullet$. Podobno iz obstoja homomorfizma $H \to G$ izpeljemo obstoj homomorfizma $H^\bullet \to G^\bullet$.

$(\Leftarrow)$ Če obstaja homomorfizem $G^\bullet \to H^\bullet$, potem imamo sledeče zaporedje homomorfizmov:
$$G \to G^\bullet \to H^\bullet \to H,$$
ki tvori homomorfizem iz $G$ v $H$. Spet lahko podobno sklepamo tudi za obratno obrnjene homomorfizme. 
\end{proof}

Dokažimo še naslednji izrek, ki zagotavlja obstoj jedra v vsakem grafu.

\begin{theorem}
\label{izr:obstoj}
Vsak graf $G$ ima jedro, ki je induciran podgraf grafa $G$ in je enoličen do izomorfizma natančno.
\end{theorem}

\begin{proof}
Naj bo $\mathcal{P}$ množica vseh takih podgrafov $G$, da obstaja homomorfizem iz $G$ vanj. Množica je neprazna (ker je identiteta homomorfizem iz $G$ v $G$) in končna (ker je graf $G$ končen, ima končno podgrafov). Torej ima vsaj en najmanjši element $H \subseteq G$ glede na inkluzijo grafov (to je kandidat za jedro grafa $G$). Zaradi minimalnosti ne obstaja noben homomorfizem iz $H$ v njegov pravi podgraf. Po trditvi \ref{trd:def} je $H$ jedro.

Dokazati moramo le še enoličnost. Denimo, da sta $H_1, H_2$ jedri grafa $G$. Označimo pripadajoča homomorfizma z $f_i \colon G \to H_i, i = 1,2$. Tedaj je $f_1|_{H_2}$ homomorfizem iz $H_2$ v $H_1$ in $f_2|_{H_1}$ homomorfizem iz $H_1$ v $H_2$. Torej sta $H_1$ in $H_2$ homomorfno ekvivalentna in zato po lemi \ref{trd:izo} izomorfna.
\end{proof}

Čeprav zdaj vemo, da jedro grafa obstaja in je do neke mere enolično, nam to ne pomaga pri ugotavljanju, ali je graf sam sebi jedro. Izkaže se celo, da je to NP-poln problem.

\begin{example}
Za ilustracijo poiščimo jedra grafov iz zgleda \ref{zgled}, ki niso sami sebi jedra.
\begin{itemize}
\item Če je $G$ dvodelni graf (z vsaj 3 vozlišči in vsaj eno povezavo) ali cikel sode dolžine, obstaja homomorfizem $G \to K_2 \subset G$ in $K_2$ je jedro ter hkrati induciran podgraf. Torej je poljubna povezava znotraj grafa $G$ njegovo jedro in seveda so vse povezave med samo izomorfne.
\item Jedro polnega grafa $K_n$ brez ene povezave je induciran podgraf, ki je izomorfen $K_{n-1}$. V zgledu \ref{zgled} smo opisali homomorfizem $K_n \to K_{n-1}$, poleg tega pa je $K_{n-1}$ jedro, saj je poln graf.
\end{itemize}
\end{example}

Že v uvodnem zgledu smo srečali nekaj družin jeder -- polni grafi, lihi cikli in liha kolesa. Vendar smo le za polne grafe znali elementarno pokazati, da so jedra. Upamo, da lahko najdemo še kakšno večjo družino jeder. S tem v mislih si oglejmo naslednjo definicijo.
% Naslednja trditev nam poda velik razred primerov jeder, vključno s polnimi grafi in lihimi cikli.

\begin{definition}
Če je $G$ takšen graf, da ima vsak njegov pravi podgraf kromatično število $< \chi(G) = k$, ga imenujemo \emph{kromatično kritični ali $k$-kritični}.
\end{definition}

\begin{example}
Oglejmo si nekaj zgledov.
\begin{itemize}
\item Vsak pravi podgraf polnega grafa $K_n (n \geq 1)$ vsebuje vsaj eno povezavo manj, zato lahko vozlišči, ki zdaj nista več sosednji pobarvamo z isto barvo in porabimo največ $n-1$ barv. Torej ima pravi podgraf kromatično število strogo manjše od $n$. Torej so polni grafi $n$-kritični.
\item Vsak pravi podgraf lihega cikla vsebuje vsaj eno povezavo manj od začetnega grafa. Torej je to podgraf poti $P_{2n+1}$, ki ima kromatično število $2 < 3 = \chi(C_{2n+1})$. Torej so lihi cikli $3$-kritični.
\end{itemize}
\end{example}

\begin{proposition}
Vsak kromatično kritični graf je jedro.
\end{proposition}

\begin{proof}
Naj bo $G$ kromatično kritični. Po posledici \ref{posl:chi} sledi, da ne obstaja noben homomorfizem iz $G$ v njegov pravi podgraf. Torej je jedro $G$ lahko le graf sam. Ker po izreku \ref{izr:obstoj} jedro obstaja, je dokaz končan. 
\end{proof}

Iz trditve sledi, da so polni grafi in lihi cikli jedra. Hkrati pa nam podaja še širši zgled jeder -- vse kromatično kritične grafe. Takšne vrste rezultat je koristen tudi pri iskanju jeder v grafu, ki ni sam sebi jedro. Če namreč najdemo homomorfizem iz grafa v nek podgrafi, ki je kromatično kritični, je to po definiciji iskano jedro. 

\section{Zaključek}

Vidimo, da so homomorfizmi precej močno orodje pri delu z grafi. Zanimivo je, da lahko iz neobstoja homomorfizma med dvema grafoma sklepamo nekaj o njunih lastnostih (npr.\ o lihi ožini in kromatičnemu številu). Presenetljivi so tudi rezultati povezani z jedri, predvsem to, da vsak graf ima enolično jedro in da lahko iz primerjave jeder dveh grafov sklepamo na morebitno homomorfno ekvivalenco grafov.

V seminarju so predstavljene le osnove dela s homomorfizmi. Poleg tega so zelo razvite teorije, ki iščejo povezave med homomorfizmi in kromatičnimi števili produktov grafov ter enolično obarvljivimi grafi. Veliko je znanega tudi o jedrih različnih družin grafov.


\begin{thebibliography}{99}
\bibitem{glavno} Godsil, Chris, and Gordon Royle. ``Algebraic graph theory, volume 207 of Graduate Texts in Mathematics.'' (2001): 103--120.
\bibitem{clanek1} Hell, Pavol, and Jaroslav Nešetřil. ``The core of a graph.'' Discrete Mathematics 109.1 (1992): 117-126.
\bibitem{clanek2} Hahn, Geňa, and Claude Tardif. ``Graph homomorphisms: structure and symmetry.'' Graph symmetry. Springer Netherlands (1997): 107-166.
\bibitem{clanek3} Foniok, Jan, and Jaroslav Nešetřil. ``Graph Theory and Combinatorics'' (2011).
%  \bibitem{Birkhoff} {G.~D.~Birkhoff}, {\emph The reducibility of maps},
%  Amer. J. Math. 35, 1913, 114--128.
%  \bibitem{Heawood} {P.~J.~Heawood}, {\emph Map--colour theorem},
%  Quart. J. Pure Appl. Math. 24, 1890, 332--338.
%  \bibitem{Heesch} {H.~Heesch}, {\emph Unter\-such\-un\-gen zum Vier\-far\-ben pro\-blem},
%  B.I.Hoch\-Schul\-scrip\-ten, 810/810a/810b, Bi\-bli\-ograph\-isches Inst\-itur, Mann\-heim--Vienna--Zurich, 1969.
%  \bibitem{Holton} {D.~A.~Holton in J.~Sheehan}, {\emph The Petersen Graph},
%  Cambridge University Press, Cambridge, 1993, 49--78.
%  \bibitem{Kempe} {A.~Kempe}, {\emph On the geographical problem of the four colours},
%  Amer, J. Math. 2, 1879, 193--200.
%  \bibitem{Robertson} {N.~Robertson, D.~P.~Sanders, P.~D.~Seymour in R.~Thomas}, {\emph The four colour theorem},
%   J. Combin. Theory Ser. B. 70, 1997, 2--44.
\end{thebibliography}